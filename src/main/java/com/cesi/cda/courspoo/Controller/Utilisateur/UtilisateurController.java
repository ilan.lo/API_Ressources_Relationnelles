package com.cesi.cda.courspoo.Controller.Utilisateur;

import com.cesi.cda.courspoo.Business.MatchBusiness;
import com.cesi.cda.courspoo.Controller.Exception.NotFoundException;
import com.cesi.cda.courspoo.Controller.Match.MatchController;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Utilisateur.Model.Utilisateur;
import com.cesi.cda.courspoo.DAO.Utilisateur.UtilisateurDAO;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class UtilisateurController {

    @GetMapping("/users/getall")
    protected List<Utilisateur> getAll() throws SQLException {
        try{
            return UtilisateurDAO.getAll();
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @GetMapping("/users/getOne/{id}")
    protected Utilisateur getOne(@PathVariable int id) throws SQLException {
        try{
            return UtilisateurDAO.getOne(id);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @PostMapping("/users/post/")
    protected int createUser(@RequestBody Utilisateur user) throws SQLException {
        try{
            return UtilisateurDAO.create(user);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }

    @PutMapping("/users/edit/{id}")
    protected Utilisateur editUser(@RequestBody Utilisateur user) throws SQLException {
        try{
            return UtilisateurDAO.edit(user);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @DeleteMapping("/users/delete/{id}")
    protected int deleteUser(@PathVariable(required = true) int id) throws SQLException {
        try{
            return UtilisateurDAO.delete(id);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @GetMapping("/users/test/")
    protected String test(@RequestParam MatchController.Status status) throws SQLException {
        //TODO

        try{
            int a =0/0;
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        return "Yes";
    }
}
