package com.cesi.cda.courspoo.Controller.Match.Model;

import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;

public class Match {
    public int id;
    String gagnant;
    String perdant;
    public String lieu;
    public String annee;
    public int sumAge;

    public Match(int id, String gagnant, String perdant, String lieu, String annee, int sumAge) {
        this.id = id;
        this.gagnant = gagnant;
        this.perdant = perdant;
        this.lieu = lieu;
        this.annee = annee;
        this.sumAge = sumAge;

    }
    private int doSumAge(){
        //
        return 10;
    }

    public String getGagnant() {
        return gagnant;
    }

    public String getPerdant() {
        return perdant;
    }

    public String getLieu() {
        return lieu;
    }

    public String getAnnee() {
        return annee;
    }

    public int getSumAge() {
        return sumAge;
    }

    public void setGagnant(String gagnant) {
        this.gagnant = gagnant;
    }

    public void setPerdant(String perdant) {
        this.perdant = perdant;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public void setSumAge(int sumAge) {
        this.sumAge = sumAge;
    }
}
