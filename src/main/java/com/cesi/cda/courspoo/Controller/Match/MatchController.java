package com.cesi.cda.courspoo.Controller.Match;

import com.cesi.cda.courspoo.Business.MatchBusiness;
import com.cesi.cda.courspoo.Controller.Exception.NotFoundException;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@RestController
public class MatchController {
    public enum Status {
        ACTIVE,
        INACTIVE,
        SUSPENDED,
        TERMINATED
    }

    @GetMapping("/api/getall")
    protected List<Match> getAll() throws SQLException {
        try{
            return MatchBusiness.getAll();
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @GetMapping("/api/getOne/{id}")
    protected Match getOne(@PathVariable int id) throws SQLException {
        try{
            return MatchBusiness.getOne(id);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @PostMapping("/api/postMatch/")
    protected int createMatch(@RequestBody Match match, int idGagnant, int idPerdant) throws SQLException {
        try{
            return MatchBusiness.create(match, idGagnant, idPerdant);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }

    @PutMapping("/api/editMatch/{id}")
    protected Match editMatch(@RequestBody Match match) throws SQLException {
        try{
            return MatchBusiness.edit(match);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @DeleteMapping("/api/editMatch/{id}")
    protected int deleteMatch(@PathVariable(required = true) int id) throws SQLException {
        try{
            return MatchBusiness.delete(id);
        }catch (SQLException e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
    }
    @GetMapping("/api/test/")
    protected String test(@RequestParam Status status) throws SQLException {
        //TODO

        try{
            int a =0/0;
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw (new NotFoundException(e));
        }
        return "Yes";
    }
}
