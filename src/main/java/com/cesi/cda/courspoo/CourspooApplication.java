package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@SpringBootApplication
public class CourspooApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourspooApplication.class, args);

		try {
			Connection connection = DatabaseController.getInstance();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM `utilisateur`");
			ResultSet result = statement.executeQuery();
			//result.first();
			//System.out.println(result.first());
			while(result.next())
			{
				System.out.println(result.getString("Nom")+" "+result.getString("Prenom"));

			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
