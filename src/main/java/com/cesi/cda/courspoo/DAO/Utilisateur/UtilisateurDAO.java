package com.cesi.cda.courspoo.DAO.Utilisateur;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.DAO.Meet.MeetDAO;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;
import com.cesi.cda.courspoo.DAO.Utilisateur.Model.Utilisateur;

import java.sql.*;
import java.util.ArrayList;
public class UtilisateurDAO {

    public static ArrayList<Utilisateur> getAll() throws SQLException {
        Connection connection = DatabaseController.getInstance();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM utilisateur");

        ArrayList<Utilisateur> users = new ArrayList<Utilisateur>();
        while (result.next()) {
            //int id, String nom, String prenom, String anneeNaissance, String nationalite
            users.add(new Utilisateur(
                    result.getInt("ID"),
                    result.getString("Nom"),
                    result.getString("Prenom"),
                    result.getString("Email"),
                    result.getString("MotDePasse"),
                    result.getBoolean("EstActif"),
                    result.getInt("IDRole"),
                    result.getString("IDCommune")
            ));
        }
        for (Utilisateur user: users) {
            System.out.println(user.Nom);
        }
        return users;
    }
    public static Utilisateur getOne(int id) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM `utilisateur` WHERE ID=?");
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        //result.first();

        Utilisateur user = new Utilisateur();

        while (result.next()) {
            //int id, String nom, String prenom, String anneeNaissance, String nationalite
            user.ID = result.getInt("ID");
            user.Nom = result.getString("Nom");
            user.Prenom = result.getString("Prenom");
            user.Email = result.getString("Email");
            user.MotDePasse = result.getString("MotDePasse");
            user.EstActif = result.getBoolean("EstActif");
            user.IDRole = result.getInt("IDRole");
            user.IDCommune = result.getString("IDCommune");

        }
        return user;
    }
    public static Utilisateur edit(Utilisateur user) throws SQLException {
        PreparedStatement statement = getPreparedStatement();
        statement.setString(1, user.Nom);
        statement.setString(2, user.Prenom);
        statement.setString(3, user.Email);
        statement.setString(4, user.MotDePasse);
        statement.setBoolean(5, user.EstActif);
        statement.setInt(6, user.IDRole);
        statement.setString(7, user.IDCommune);
        statement.setInt(8, user.ID);
        statement.executeUpdate();

        Utilisateur checkUser = UtilisateurDAO.getOne(user.ID);
        return checkUser;
    }

    private static PreparedStatement getPreparedStatement() throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(

                "UPDATE utilisateur " +
                        "SET Nom = ? , " +
                        " Prenom = ?, " +
                        " Email = ?, " +
                        " MotDePasse = ?, " +
                        " EstActif = ?, " +
                        " IDRole = ?, " +
                        " IDCommune = ? " +
                        "WHERE ID = ?"
        );
        return statement;
    }

    public static int delete(int id) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM  utilisateur" +
                        "WHERE ID = ?"
        );
        statement.setInt(1, id);
        return statement.executeUpdate();
    }
    public static Utilisateur getLast() throws SQLException {
        String query = "SELECT * FROM `utilisateur` ORDER BY `ID` DESC";
        Connection con = DatabaseController.getInstance();
        PreparedStatement stmp = con.prepareStatement(query);
        ResultSet result = stmp.executeQuery();
        result.first();
        return new Utilisateur(
                result.getInt("ID"),
                result.getString("Nom"),
                result.getString("Prenom"),
                result.getString("Email"),
                result.getString("MotDePasse"),
                result.getBoolean("EstActif"),
                result.getInt("IDRole"),
                result.getString("IDCommune")
        );
    }
    public static int create(Utilisateur user) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO utilisateur (`Nom`, `Prenom`, `Email`, `MotDePasse`, `EstActif`, `IDRole`, `IDCommune`) VALUES (?, ?, ?, ?, ?, ?, ?)"
        );
        statement.setString(1, user.Nom);
        statement.setString(2, user.Prenom);
        statement.setString(3, user.Email);
        statement.setString(4, user.MotDePasse);
        statement.setBoolean(5, user.EstActif);
        statement.setInt(6, user.IDRole);
        statement.setString(7, user.IDCommune);
        return statement.executeUpdate();
    }
}
