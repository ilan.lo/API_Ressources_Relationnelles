package com.cesi.cda.courspoo.DAO.Utilisateur.Model;

public class Utilisateur {
    public int ID;
    public String Nom;
    public String Prenom;
    public String Email;
    public String MotDePasse;
    public Boolean EstActif;
    public int IDRole;
    public String IDCommune;

    public Utilisateur(int ID, String Nom, String Prenom, String Email, String MotDePasse, boolean EstActif, int IDRole, String IDCommune) {
        this.ID = ID;
        this.Nom = Nom;
        this.Prenom = Prenom;
        this.Email = Email;
        this.MotDePasse = MotDePasse;
        this.EstActif = EstActif;
        this.IDRole = IDRole;
        this.IDCommune = IDCommune;
    }

    public Utilisateur() {
        this.ID = 0;
        this.Nom = "default";
        this.Prenom = "default";
        this.Email = "default";
        this.MotDePasse = "default";
        this.EstActif = true;
        this.IDRole = 1;
        this.IDCommune = "default";
    }


}
