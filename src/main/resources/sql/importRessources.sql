CREATE DATABASE  IF NOT EXISTS `ressources_cube` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ressources_cube`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ressources_cube
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abonnement`
--

DROP TABLE IF EXISTS `abonnement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abonnement` (
  `IDUtilisateur` int NOT NULL,
  `IDAbonnement` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDAbonnement`),
  KEY `IDAbonnement` (`IDAbonnement`),
  CONSTRAINT `abonnement_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `abonnement_ibfk_2` FOREIGN KEY (`IDAbonnement`) REFERENCES `utilisateur` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abonnement`
--

LOCK TABLES `abonnement` WRITE;
/*!40000 ALTER TABLE `abonnement` DISABLE KEYS */;
/*!40000 ALTER TABLE `abonnement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activite`
--

DROP TABLE IF EXISTS `activite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activite` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DateDebut` datetime DEFAULT NULL,
  `DateFin` datetime DEFAULT NULL,
  `ID_IDRessource` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_IDRessource` (`ID_IDRessource`),
  CONSTRAINT `activite_ibfk_1` FOREIGN KEY (`ID_IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activite`
--

LOCK TABLES `activite` WRITE;
/*!40000 ALTER TABLE `activite` DISABLE KEYS */;
/*!40000 ALTER TABLE `activite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorieressource`
--

DROP TABLE IF EXISTS `categorieressource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorieressource` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorieressource`
--

LOCK TABLES `categorieressource` WRITE;
/*!40000 ALTER TABLE `categorieressource` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorieressource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commentaire` (
  `ID` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `donneescommentaire` (`ID`),
  CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaire`
--

LOCK TABLES `commentaire` WRITE;
/*!40000 ALTER TABLE `commentaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `commentaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commune`
--

DROP TABLE IF EXISTS `commune`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commune` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `CodePostal` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commune`
--

LOCK TABLES `commune` WRITE;
/*!40000 ALTER TABLE `commune` DISABLE KEYS */;
/*!40000 ALTER TABLE `commune` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Extension` varchar(50) NOT NULL,
  `NomHash` varchar(100) NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donneescommentaire`
--

DROP TABLE IF EXISTS `donneescommentaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `donneescommentaire` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DateCreation` datetime NOT NULL,
  `Contenu` text NOT NULL,
  `EstActif` tinyint(1) NOT NULL,
  `IDUtilisateur` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDUtilisateur` (`IDUtilisateur`),
  CONSTRAINT `donneescommentaire_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donneescommentaire`
--

LOCK TABLES `donneescommentaire` WRITE;
/*!40000 ALTER TABLE `donneescommentaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `donneescommentaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historique`
--

DROP TABLE IF EXISTS `historique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `historique` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DateEvenement` datetime NOT NULL,
  `IDUtilisateur` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDUtilisateur` (`IDUtilisateur`),
  CONSTRAINT `historique_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historique`
--

LOCK TABLES `historique` WRITE;
/*!40000 ALTER TABLE `historique` DISABLE KEYS */;
/*!40000 ALTER TABLE `historique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historiqueconsultation`
--

DROP TABLE IF EXISTS `historiqueconsultation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `historiqueconsultation` (
  `ID` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `historiqueconsultation_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `historique` (`ID`),
  CONSTRAINT `historiqueconsultation_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historiqueconsultation`
--

LOCK TABLES `historiqueconsultation` WRITE;
/*!40000 ALTER TABLE `historiqueconsultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `historiqueconsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historiquerecherche`
--

DROP TABLE IF EXISTS `historiquerecherche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `historiquerecherche` (
  `ID` int NOT NULL,
  `RechercheEffectuee` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `historiquerecherche_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `historique` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historiquerecherche`
--

LOCK TABLES `historiquerecherche` WRITE;
/*!40000 ALTER TABLE `historiquerecherche` DISABLE KEYS */;
/*!40000 ALTER TABLE `historiquerecherche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscriptionactivite`
--

DROP TABLE IF EXISTS `inscriptionactivite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inscriptionactivite` (
  `IDUtilisateur` int NOT NULL,
  `IDActivite` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDActivite`),
  KEY `IDActivite` (`IDActivite`),
  CONSTRAINT `inscriptionactivite_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `inscriptionactivite_ibfk_2` FOREIGN KEY (`IDActivite`) REFERENCES `activite` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscriptionactivite`
--

LOCK TABLES `inscriptionactivite` WRITE;
/*!40000 ALTER TABLE `inscriptionactivite` DISABLE KEYS */;
/*!40000 ALTER TABLE `inscriptionactivite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lien_type_relation`
--

DROP TABLE IF EXISTS `lien_type_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lien_type_relation` (
  `IDRessource` int NOT NULL,
  `IDTypeRelation` int NOT NULL,
  PRIMARY KEY (`IDRessource`,`IDTypeRelation`),
  KEY `IDTypeRelation` (`IDTypeRelation`),
  CONSTRAINT `lien_type_relation_ibfk_1` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`),
  CONSTRAINT `lien_type_relation_ibfk_2` FOREIGN KEY (`IDTypeRelation`) REFERENCES `typerelation` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lien_type_relation`
--

LOCK TABLES `lien_type_relation` WRITE;
/*!40000 ALTER TABLE `lien_type_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `lien_type_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likemessage`
--

DROP TABLE IF EXISTS `likemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likemessage` (
  `IDUtilisateur` int NOT NULL,
  `IDCommentaire` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDCommentaire`),
  KEY `IDCommentaire` (`IDCommentaire`),
  CONSTRAINT `likemessage_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `likemessage_ibfk_2` FOREIGN KEY (`IDCommentaire`) REFERENCES `donneescommentaire` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likemessage`
--

LOCK TABLES `likemessage` WRITE;
/*!40000 ALTER TABLE `likemessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `likemessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reponsecommentaire`
--

DROP TABLE IF EXISTS `reponsecommentaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reponsecommentaire` (
  `ID` int NOT NULL,
  `ID_IDCommentaire` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID_IDCommentaire` (`ID_IDCommentaire`),
  CONSTRAINT `reponsecommentaire_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `donneescommentaire` (`ID`),
  CONSTRAINT `reponsecommentaire_ibfk_2` FOREIGN KEY (`ID_IDCommentaire`) REFERENCES `commentaire` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reponsecommentaire`
--

LOCK TABLES `reponsecommentaire` WRITE;
/*!40000 ALTER TABLE `reponsecommentaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponsecommentaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressource`
--

DROP TABLE IF EXISTS `ressource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ressource` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) NOT NULL,
  `DateCreation` varchar(250) NOT NULL,
  `Contenu` text NOT NULL,
  `IDStatut` int NOT NULL,
  `IDAuteur` int NOT NULL,
  `IDCategorieRessource` int NOT NULL,
  `IDTypeRessource` int NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDStatut` (`IDStatut`),
  KEY `IDAuteur` (`IDAuteur`),
  KEY `IDCategorieRessource` (`IDCategorieRessource`),
  KEY `IDTypeRessource` (`IDTypeRessource`),
  CONSTRAINT `ressource_ibfk_1` FOREIGN KEY (`IDStatut`) REFERENCES `statut` (`ID`),
  CONSTRAINT `ressource_ibfk_2` FOREIGN KEY (`IDAuteur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `ressource_ibfk_3` FOREIGN KEY (`IDCategorieRessource`) REFERENCES `categorieressource` (`ID`),
  CONSTRAINT `ressource_ibfk_4` FOREIGN KEY (`IDTypeRessource`) REFERENCES `typeressource` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressource`
--

LOCK TABLES `ressource` WRITE;
/*!40000 ALTER TABLE `ressource` DISABLE KEYS */;
/*!40000 ALTER TABLE `ressource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressourceenregistre`
--

DROP TABLE IF EXISTS `ressourceenregistre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ressourceenregistre` (
  `IDUtilisateur` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDRessource`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `ressourceenregistre_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `ressourceenregistre_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressourceenregistre`
--

LOCK TABLES `ressourceenregistre` WRITE;
/*!40000 ALTER TABLE `ressourceenregistre` DISABLE KEYS */;
/*!40000 ALTER TABLE `ressourceenregistre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressourceexploite`
--

DROP TABLE IF EXISTS `ressourceexploite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ressourceexploite` (
  `IDUtilisateur` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDRessource`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `ressourceexploite_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `ressourceexploite_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressourceexploite`
--

LOCK TABLES `ressourceexploite` WRITE;
/*!40000 ALTER TABLE `ressourceexploite` DISABLE KEYS */;
/*!40000 ALTER TABLE `ressourceexploite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressourcefavoris`
--

DROP TABLE IF EXISTS `ressourcefavoris`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ressourcefavoris` (
  `IDUtilisateur` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDRessource`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `ressourcefavoris_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `ressourcefavoris_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressourcefavoris`
--

LOCK TABLES `ressourcefavoris` WRITE;
/*!40000 ALTER TABLE `ressourcefavoris` DISABLE KEYS */;
/*!40000 ALTER TABLE `ressourcefavoris` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressourcenonexploite`
--

DROP TABLE IF EXISTS `ressourcenonexploite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ressourcenonexploite` (
  `IDUtilisateur` int NOT NULL,
  `IDRessource` int NOT NULL,
  PRIMARY KEY (`IDUtilisateur`,`IDRessource`),
  KEY `IDRessource` (`IDRessource`),
  CONSTRAINT `ressourcenonexploite_ibfk_1` FOREIGN KEY (`IDUtilisateur`) REFERENCES `utilisateur` (`ID`),
  CONSTRAINT `ressourcenonexploite_ibfk_2` FOREIGN KEY (`IDRessource`) REFERENCES `ressource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressourcenonexploite`
--

LOCK TABLES `ressourcenonexploite` WRITE;
/*!40000 ALTER TABLE `ressourcenonexploite` DISABLE KEYS */;
/*!40000 ALTER TABLE `ressourcenonexploite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'User');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statut`
--

DROP TABLE IF EXISTS `statut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statut` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statut`
--

LOCK TABLES `statut` WRITE;
/*!40000 ALTER TABLE `statut` DISABLE KEYS */;
INSERT INTO `statut` VALUES (1,'OK');
/*!40000 ALTER TABLE `statut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typerelation`
--

DROP TABLE IF EXISTS `typerelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `typerelation` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typerelation`
--

LOCK TABLES `typerelation` WRITE;
/*!40000 ALTER TABLE `typerelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `typerelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typeressource`
--

DROP TABLE IF EXISTS `typeressource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `typeressource` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typeressource`
--

LOCK TABLES `typeressource` WRITE;
/*!40000 ALTER TABLE `typeressource` DISABLE KEYS */;
/*!40000 ALTER TABLE `typeressource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `MotDePasse` varchar(200) NOT NULL,
  `EstActif` tinyint(1) NOT NULL,
  `IDRole` int NOT NULL,
  `IDCommune` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDRole` (`IDRole`),
  CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`IDRole`) REFERENCES `role` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (1,'Loore','Ilan','ilan.loore@gmail','testNotSecured',1,1,'2'),(2,'greg','string','string','string',1,1,'string');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-05 11:58:24
